#!/usr/bin/env python3

import re
import json
import zipfile

from base64 import encodebytes

import inkex
from inkex.base import TempDirMixin
from inkex.elements import Layer, Group, TextElement, Rectangle, Image
from inkex.transforms import Transform
from inkex.colors import Color

class SketchInput(TempDirMixin, inkex.InputExtension):
    """
    Read in sketch43 zip/json format and output svg.
    """
    snake_case = re.compile(r'(?<!^)(?=[A-Z])')

    def _call(self, prefix, key, *args, **kwargs):
        default = kwargs.pop('default', None)
        name = prefix + self.snake_case.sub('_', key).lower()
        method = getattr(self, name, None)
        if method:
            return method(*args, **kwargs)
        elif default is not None:
            return default
        raise KeyError(f"Can't call: {name}")

    def get_file(self, input_file):
        """Convert streams to filenames if needed"""
        if not isinstance(input_file, str):
            if hasattr(input_file, 'buffer'): # stdin
                input_file = input_file.buffer
        return input_file

    def load(self, stream):
        """Load skz file format and return svg document"""
        doc = self.get_template(width=1, height=1)
        svg = doc.getroot()
        # Load sketch file
        self.skz = zipfile.ZipFile(self.get_file(stream), 'r')
        self.skd = json.loads(self.skz.read('document.json'))
        for page_ref in self.skd['pages']:
            page = json.loads(self.skz.read(page_ref['_ref'] + '.json'))
            self.load_page(svg.add(Layer.new(page['name'])), page)
        return doc

    def load_page(self, svg, page):
        """Load a single page"""
        svg.add(*list(self.load_layers(page)))

    def load_layers(self, page):
        """Load all layers recursively"""
        for layer in page.pop('layers', []):
            elem = self._call('get_', layer['_class'], layer)
            self.apply_common(layer, elem)
            # TODO: Could clean layer dictionary and report unusual remaining pieces.
            # print(json.dumps(layer, indent=2))
            yield elem

    def get_group(self, layer):
        svg = Group.new(layer['name'])
        svg.add(*list(self.load_layers(layer)))
        return svg

    def get_artboard(self, layer):
        svg = Layer.new(layer['name'])
        svg.add(*list(self.load_layers(layer)))
        return svg

    def get_text(self, layer):
        """Process text class"""
        elem = TextElement()
        elem.text = layer['attributedString'].pop('string')
        bit = layer['attributedString']['attributes'][0]['attributes']

        bit.pop('textStyleVerticalAlignmentKey', None) # TODO: vertical text?
        layer.pop('automaticallyDrawOnUnderlyingPath', None) # No idea
        layer.pop('dontSynchroniseWithSymbol', None) # No idea

        font = bit['MSAttributedStringFontAttribute']['attributes']
        elem.style['font-family'] = font.pop('name')
        elem.style['font-size'] = font.pop('size')
        elem.style.set_color(self.get_color(bit.pop('MSAttributedStringColorAttribute')))
        return elem

    def get_shape_group(self, layer):
        elem = Group()
        layer.pop('groupLayout', None) # No idea
        for child in self.load_layers(layer):
            elem.append(child)
        return elem

    def get_rectangle(self, layer):
        elem = Rectangle()

        # This contains a whole bunch of rubbish related to the
        # rectangle, which may or may not translate into svg.
        # But it doesn't contain any actual size information.
        layer.pop('points')

        for key in ('x', 'y', 'width', 'height'):
            elem.set(key, layer['frame'].pop(key))
        return elem

    def get_bitmap(self, layer):
        elem = Image()
        name = layer.pop('image')['_ref']
        img = self.skz.read(name)
        file_type = {
            'png': 'image/png',
        }[name.rsplit('.', 1)[-1]]
        elem.set('xlink:href', 'data:{};base64,{}'.format(
            file_type, encodebytes(img).decode('ascii')))
        for key in ('x', 'y', 'width', 'height'):
            elem.set(key, layer['frame'].pop(key))
        return elem

    def apply_common(self, layer, elem):
        """Apply common attributes to the element"""
        # Remove Garbage
        for key in ('maintainScrollPosition', 'layerListExpandedType', 'booleanOperation',
                    '_class', 'nameIsFixed', 'shouldBreakMaskChain', 'exportOptions'):
            layer.pop(key, None)

        # Tranformations
        if layer.pop('isFixedToViewport', False):
            self.msg("isFixedToViewport: We don't know what this does!")
        if layer.pop('isFlippedHorizontal', False):
            elem.transform @= Transform(scale=(-1, 1))
        if layer.pop('isFlippedVertical', False):
            elem.transform @= Transform(scale=(1, -1))
        elem.transform @= Transform(rotate=layer.pop('rotation', 0))

        # TODO:
        layer.pop('resizingConstraint')
        layer.pop('resizingType')
        layer.pop('clippingMaskMode')
        layer.pop('hasClippingMask')

        elem.set_id(layer.pop('do_objectID'))
        elem.label = layer.pop('name', None)
        elem.set_sensitive(not layer.pop('isLocked', False))

        # Style properties
        if not layer.pop('isVisible', True):
            elem.style.set('display', 'none')
        self.apply_style(layer.pop('style', {}), elem.style)
        return elem

    def apply_style(self, st_layer, st_elem):
        """Apply common style items"""
        st_layer.pop('blur', {}) # TODO: Apply blur filter
        st_layer.pop('borderOptions', {}) # TODO: Decode border
        st_layer.pop('borders', []) # TODO: find out WTF
        st_layer.pop('colorControls', {}) # This looks like the color
        for fill in st_layer.pop('fills', []):
            if not fill.pop('isEnabled', True):
                continue
            fill.pop('fillType', None) # ?
            st_elem.set_color(self.get_color(fill.pop('color')))
            fill.pop('gradient', None) #TODO: gradient fills

    def get_color(self, _sk):
        return Color([float(_sk[v]) for v in ('red', 'green', 'blue', 'alpha')], 'rgba')


if __name__ == '__main__':
    SketchInput().run()
