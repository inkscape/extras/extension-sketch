# coding=utf-8
"""
Basic full round input test
"""

from sketch_43 import SketchInput
from inkex.tester import ComparisonMixin, TestCase

class TestSketchInput(ComparisonMixin, TestCase):
    effect_class = SketchInput
    compare_file = ['groups.sketch', 'images.sketch']
    comparisons = [()]
